/*******************************************************************************
 * Copyright 2023 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <pthread.h>
#include <stdio.h>
#include <signal.h>
#include <getopt.h>
#include <time.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <errno.h>
#include <stdbool.h>

#include <opencv2/core/hal/interface.h>
#include <opencv2/features2d.hpp>
#include <opencv2/imgproc.hpp>

#include <modal_start_stop.h>
#include <modal_pipe_client.h>
#include <modal_pipe_server.h>
#include <modalcv/pyr_fpx.h>


#define PROCESS_NAME	"voxl-test-feature-extraction"
#define SUFFIX			"_fpx"

static mcv_cvp_pyr_fpx_handle handle;
static mcv_cvp_pyr_fpx_t output_pyr_fpx;
static mcv_cvp_pyr_fpx_config_t pyr_fpx_config;
static char pipe_path[MODAL_PIPE_MAX_PATH_LEN];
static char out_pipe_name[MODAL_PIPE_MAX_PATH_LEN];
static int en_preprocessing = 0;
static int robustness = -1;
static int score_threshold = -1;
static int en_timing = 0;


static void _print_usage(void)
{
printf("\n\
\n\
run live feature extraction on a pipe and publish a new pipe with overlay\n\
\n\
-h, --help                   print this help message\n\
-p, --en_preprocessing       enable preprocessing for non-normalized image streams\n\
-r, --robustness {val}       optionally change robustness value (default 70)\n\
-s, --score_threshold {val}  optionally change score threshold (default 50)\n\
-t, --en_timing              enable printing of function execution time\n\
\n\
typical usage:\n\
/# voxl-test-feature-extraction -p tracking\n\
\n");
	return;
}



static int _parse_opts(int argc, char* argv[])
{
	static struct option long_options[] =
	{
		{"help",               no_argument,              0, 'h'},
		{"en_preprocessing",   no_argument,              0, 'p'},
		{"robustness",         required_argument,        0, 'r'},
		{"score_threshold",    required_argument,        0, 's'},
		{"en_timing",          no_argument,              0, 't'},
		{0, 0, 0, 0}
	};


	while(1){
		int option_index = 0;
		int c = getopt_long(argc, argv, "hpr:s:t", long_options, &option_index);

		if(c == -1) break; // Detect the end of the options.

		switch(c){
		case 0:
			// for long args without short equivalent that just set a flag
			// nothing left to do so just break.
			if (long_options[option_index].flag != 0) break;
			break;
		case 'h':
			_print_usage();
			exit(0);
			break;
		case 'p':
			en_preprocessing = 1;
			break;
		case 'r':
			robustness = atoi(optarg);
			break;
		case 's':
			score_threshold = atoi(optarg);
			break;
		case 't':
			en_timing = 1;
			break;
		default:
			_print_usage();
			exit(0);
		}
	}

	// scan through the non-flagged arguments for the desired pipe
	for(int i=optind; i<argc; i++){
		if(pipe_path[0]!=0){
			fprintf(stderr, "ERROR: Please specify only one pipe\n");
			_print_usage();
			exit(-1);
		}
		if(pipe_expand_location_string(argv[i], pipe_path)<0){
			fprintf(stderr, "ERROR: Invalid pipe name: %s\n", argv[i]);
			exit(-1);
		}
		unsigned int maxlen = MODAL_PIPE_MAX_NAME_LEN-strlen(SUFFIX)-1;
		if(strlen(argv[i]) >= maxlen){
			fprintf(stderr, "pipe name too long, should not exceed %d, otherwise no room to add suffix\n", maxlen);
			exit(-1);
		}
		strcpy(out_pipe_name, argv[i]);
		strcat(out_pipe_name, SUFFIX);
	}

	// make sure a pipe was given
	if(pipe_path[0] == 0){
		fprintf(stderr, "ERROR: There must be one input pipe argument given\n");
		_print_usage();
		exit(-1);
	}

	return 0;
}


static int _init(int w, int h)
{
	// configure everything
	pyr_fpx_config = mcv_cvp_pyr_fpx_default_config();
	pyr_fpx_config.width = w;
	pyr_fpx_config.height = h;


	// these are the only two things really worth playign with
	if(score_threshold > 0){
		pyr_fpx_config.score_threshold = score_threshold;
	}
	if(robustness > 0){
		pyr_fpx_config.robustness = robustness;
	}


	// create a pyr_fpx handle from our configuration
	handle = mcv_cvp_pyr_fpx_init(pyr_fpx_config);
	if(NULL == handle) {
		fprintf(stderr, "ERROR creating pyr fpx handle\n");
		return -1;
	}

	// create an output object
	if(mcv_cvp_create_pyr_fpx(handle, &output_pyr_fpx)) {
		fprintf(stderr, "ERROR creating feature extraction pyramid\n");
		return -1;
	}

	// make a new pipe to output the overlay on
	pipe_info_t info = { \
		"",							// name
		"",							// location
		"camera_image_metadata_t",	// type
		PROCESS_NAME,				// server_name
		64*1024*1024,				// size_bytes
		0							// server_pid
	};

	strcpy(info.name, out_pipe_name);

	if(pipe_server_create(0, info, 0)){
		return -1;
	}

	printf("open voxl-portal to view the %s pipe\n", out_pipe_name);

	return 0;
}

/**
 * @brief timing helper function
 * used across mai projects
 *
 * @return int64_t monotonic time in nanoseconds
 */
static int64_t _apps_time_monotonic_ns() {
	struct timespec ts;
	if (clock_gettime(CLOCK_MONOTONIC, &ts)) {
		fprintf(stderr, "ERROR calling clock_gettime\n");
		return -1;
	}
	return (int64_t)ts.tv_sec * 1000000000 + (int64_t)ts.tv_nsec;
}

// called whenever we disconnect from the server
static void _disconnect_cb(__attribute__((unused)) int ch, __attribute__((unused)) void* context)
{
	printf("Server Disconnected\n");
	return;
}

static void _connect_cb(__attribute__((unused)) int ch, __attribute__((unused)) void* context)
{
	printf("connected\n");
	return;
}



void do_preprocessing(const cv::Mat& img)
{

	// smooth out input image
	GaussianBlur(img, img, cv::Size(5, 5), 0);

	// Resize the image
	int scale = 32;
	cv::Mat resizedImg;
	cv::resize(img, resizedImg, cv::Size(img.cols/scale, img.rows/scale));

	// blur the downsized image
	cv::Mat blurredImg;
	cv::GaussianBlur(resizedImg, blurredImg, cv::Size(9, 9), 0);

	// Upscale the blurred image back to the original size
	cv::Mat upscaledBlurredImg;
	cv::resize(blurredImg, upscaledBlurredImg, img.size());

	// Convert upscaled blurred image to 16-bit
	cv::Mat upscaledBlurred16bit;
	upscaledBlurredImg.convertTo(upscaledBlurred16bit, CV_16U, 2);  // Convert blurred image to 16-bit

	// Convert the original image to 16-bit
	cv::Mat img16bit;
	img.convertTo(img16bit, CV_16U, 256);  // Convert original image to 16-bit

	// Perform element-wise division (note: OpenCV does not support direct division of different types)
	cv::Mat dividedImg;
	cv::divide(img16bit, upscaledBlurred16bit, dividedImg, 1.0, CV_16U);  // Using CV_16U to keep results in 16-bit space

	// Normalize to 8-bit range
	dividedImg.convertTo(img, CV_8U, 1.0);

	return;
}



// camera helper callback whenever a frame arrives
static void _helper_cb(__attribute__((unused)) int ch, camera_image_metadata_t meta, char* frame, __attribute__((unused)) void* context)
{
	if(meta.format != IMAGE_FORMAT_RAW8){
		fprintf(stderr, "this test code only works with 8-bit images\n");
		return;
	}

	static int has_init = 0;
	if(!has_init && _init(meta.width, meta.height)) return;
	has_init = 1;

	// convert to opencv mat for convenience
	// this does not memcopy the image, only wrap it with an opencv header
	cv::Mat img(meta.height, meta.width, CV_8UC1, frame);


	// do the pre-processing step, only needed on image stream that are not normalized already
	if(en_preprocessing){
		int64_t start = _apps_time_monotonic_ns();
		do_preprocessing(img);
		int64_t end = _apps_time_monotonic_ns();
		if(en_timing) printf("preprocess: %6.2fms\n", (double)(end - start) / 1000000);
	}


	// run the pyr fpx process
	int64_t start = _apps_time_monotonic_ns();
	mcv_cvp_pyr_fpx_process(handle, (uint8_t*)frame, &output_pyr_fpx);
	int64_t end = _apps_time_monotonic_ns();
	if(en_timing) printf("pyr_fpx:    %6.2fms\n", (double)(end - start) / 1000000);


	// no need to draw the overlay if there are no clients
	if(pipe_server_get_num_clients(0)<1) return;


	// draw the overlay
	cv::Mat overlay;
	cv::cvtColor(img, overlay, cv::COLOR_GRAY2RGB);
	for (size_t oct = 0; oct < pyr_fpx_config.n_octaves; oct++) {
		for (int i = 0; i < output_pyr_fpx.n_points[oct]; i++) {
			mcv_cvp_fpx_feature_t feature = output_pyr_fpx.fpx[oct][i];
			int feature_x = feature.x * pow(2, oct);  // upscale
			int feature_y = feature.y * pow(2, oct);  // upscale
			int radius = 2 + 2*oct;
			int thickness = 1;

			cv::Scalar color;
			if (oct == 0) {
				color = cv::Scalar(255, 0, 0);
			} else if (oct == 1) {
				color = cv::Scalar(0, 255, 0);
			} else {
				color = cv::Scalar(0, 0, 255);
			}

			cv::Rect rect(feature_x - radius, feature_y - radius,
						  radius * 2, radius * 2);
			cv::rectangle(overlay, rect, color, thickness, 1, 0);
		}
	}

	camera_image_metadata_t color_meta = meta;
	color_meta.format = IMAGE_FORMAT_RGB;
	color_meta.size_bytes = meta.size_bytes * 3;
	pipe_server_write_camera_frame(0, color_meta, overlay.data);

	return;
}




int main(int argc, char* argv[])
{
	// check for options
	if(_parse_opts(argc, argv)) return -1;

	// set some basic signal handling for safe shutdown.
	// quitting without cleanup up the pipe can result in the pipe staying
	// open and overflowing, so always cleanup properly!!!
	enable_signal_handler();
	main_running = 1;


	// set up all our MPA callbacks
	pipe_client_set_camera_helper_cb(0, _helper_cb, NULL);
	pipe_client_set_disconnect_cb(0, _disconnect_cb, NULL);
	pipe_client_set_connect_cb(0, _connect_cb, NULL);
	printf("waiting for server at %s\n", pipe_path);
	pipe_client_open(0, pipe_path, PROCESS_NAME, EN_PIPE_CLIENT_CAMERA_HELPER, 0);

	// keep going until the  signal handler sets the running flag to 0
	while(main_running) usleep(500000);

	// all done, signal pipe read threads to stop
	pipe_client_close_all();
	pipe_server_close_all();
	printf("\nclosing and exiting\n");

	return 0;
}
