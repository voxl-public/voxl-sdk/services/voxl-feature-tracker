/*******************************************************************************
 * Copyright 2023 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <pthread.h>
#include <stdio.h>
#include <signal.h>
#include <getopt.h>
#include <time.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <errno.h>
#include <stdbool.h>

#include <opencv2/core/hal/interface.h>
#include <opencv2/features2d.hpp>
#include <opencv2/imgproc.hpp>

#include <modal_start_stop.h>
#include <modal_pipe_client.h>
#include <modal_pipe_server.h>
#include <modalcv/opt_flow.h>
#include <modalcv/pyr.h>


#define PROCESS_NAME	"voxl-test-optic-flow"
#define SUFFIX			"_flow"

static mcv_cvp_opt_flow_handle opt_flow_handle;
static mcv_cvp_pyr_handle pyr_handle;
static mcv_cvp_opt_flow_output_t opt_flow_output;
static mcv_cvp_pyr_t pyr_output;
static mcv_cvp_opt_flow_config_t opt_flow_config;
static char pipe_path[MODAL_PIPE_MAX_PATH_LEN];
static char out_pipe_name[MODAL_PIPE_MAX_PATH_LEN];
static int optic_flow_octave = 1;
static int en_preprocessing = 0;
static int en_timing = 0;


static void _print_usage(void)
{
printf("\n\
\n\
run live optic flow on a pipe and publish a new pipe with overlay\n\
\n\
-h, --help                   print this help message\n\
-o  --ocatve                 set the octave that optic flow is performed on\n\
-p, --en_preprocessing       enable preprocessing for non-normalized image streams\n\
-t, --en_timing              enable printing of function execution time\n\
\n\
typical usage:\n\
/# voxl-test-optic-flow -p tracking\n\
\n");
	return;
}


static int _parse_opts(int argc, char* argv[])
{
	static struct option long_options[] =
	{
		{"help",               no_argument,              0, 'h'},
		{"octave",             required_argument,        0, 'o'},
		{"en_preprocessing",   no_argument,              0, 'p'},
		{"en_timing",          no_argument,              0, 't'},
		{0, 0, 0, 0}
	};


	while(1){
		int option_index = 0;
		int c = getopt_long(argc, argv, "ho:pt", long_options, &option_index);

		if(c == -1) break; // Detect the end of the options.

		switch(c){
		case 0:
			// for long args without short equivalent that just set a flag
			// nothing left to do so just break.
			if (long_options[option_index].flag != 0) break;
			break;
		case 'h':
			_print_usage();
			exit(0);
			break;
		case 'o':
			optic_flow_octave = atoi(optarg);
			break;
		case 'p':
			en_preprocessing = 1;
			break;
		case 't':
			en_timing = 1;
			break;
		default:
			_print_usage();
			exit(0);
		}
	}

	// scan through the non-flagged arguments for the desired pipe
	for(int i=optind; i<argc; i++){
		if(pipe_path[0]!=0){
			fprintf(stderr, "ERROR: Please specify only one pipe\n");
			_print_usage();
			exit(-1);
		}
		if(pipe_expand_location_string(argv[i], pipe_path)<0){
			fprintf(stderr, "ERROR: Invalid pipe name: %s\n", argv[i]);
			exit(-1);
		}
		unsigned int maxlen = MODAL_PIPE_MAX_NAME_LEN-strlen(SUFFIX)-1;
		if(strlen(argv[i]) >= maxlen){
			fprintf(stderr, "pipe name too long, should not exceed %d, otherwise no room to add suffix\n", maxlen);
			exit(-1);
		}
		strcpy(out_pipe_name, argv[i]);
		strcat(out_pipe_name, SUFFIX);
	}

	// make sure a pipe was given
	if(pipe_path[0] == 0){
		fprintf(stderr, "ERROR: There must be one input pipe argument given\n");
		_print_usage();
		exit(-1);
	}

	return 0;
}

static int _init(int w, int h)
{
	// configure everything
	opt_flow_config.width = w >> optic_flow_octave;
	opt_flow_config.height = h >> optic_flow_octave;
	opt_flow_config.en_stats = 1;
	opt_flow_config.input_ubwc = 0;

	// create a opt_flow handle from our configuration
	opt_flow_handle = mcv_cvp_opt_flow_init(opt_flow_config);
	if(NULL == opt_flow_handle) {
		fprintf(stderr, "ERROR creating opt flow handle\n");
		return -1;
	}

	// create an output object
	if(mcv_cvp_create_opt_flow_output(opt_flow_handle, &opt_flow_output)) {
		fprintf(stderr, "ERROR creating feature extraction pyramid\n");
		return -1;
	}

	// configuration for pyramid downscaler
	mcv_cvp_pyr_config_t pyr_config;
	pyr_config.width               = w;
	pyr_config.height              = h;
	pyr_config.n_octaves           = optic_flow_octave + 1;
	pyr_config.n_scales_per_octave = 1;
	pyr_config.output_ubwc         = 0;

	// gets a handle for pyramid downscaler process
	pyr_handle = mcv_cvp_pyr_init(pyr_config);
	if(NULL == pyr_handle) {
		fprintf(stderr, "ERROR creating pyramid handle\n");
		return -1;
	}

	// create output structure
	if (mcv_cvp_create_pyr(pyr_handle, &pyr_output)) {
		fprintf(stderr, "ERROR creating PYR output object\n");
		return -1;
	}

	// make a new pipe to output the overlay on
	pipe_info_t info = { \
		"",							// name
		"",							// location
		"camera_image_metadata_t",	// type
		PROCESS_NAME,				// server_name
		64*1024*1024,				// size_bytes
		0							// server_pid
	};

	strcpy(info.name, out_pipe_name);

	if(pipe_server_create(0, info, 0)){
		return -1;
	}

	printf("open voxl-portal to view the %s pipe\n", out_pipe_name);

	return 0;
}

static int _deinit(void) {
	// optic flow cleanup
	if (mcv_cvp_destroy_opt_flow_output(opt_flow_handle, &opt_flow_output)) {
		fprintf(stderr, "ERROR destorying optic flow object\n");
		return -1;
	}
	if (mcv_cvp_opt_flow_deinit(opt_flow_handle)) {
		fprintf(stderr, "ERROR deinitializing optic flow\n");
		return -1;
	}

	// cleanup for pyramid extraction
	if(mcv_cvp_destroy_pyr(pyr_handle, &pyr_output)) {
		fprintf(stderr, "ERROR destorying pyramid object\n");
		return -1;
	}
	if(mcv_cvp_pyr_deinit(pyr_handle)) {
		fprintf(stderr, "ERROR deinitializing pyramid\n");
		return -1;
	}
}

/**
 * @brief timing helper function
 * used across mai projects
 *
 * @return int64_t monotonic time in nanoseconds
 */
static int64_t _apps_time_monotonic_ns() {
	struct timespec ts;
	if (clock_gettime(CLOCK_MONOTONIC, &ts)) {
		fprintf(stderr, "ERROR calling clock_gettime\n");
		return -1;
	}
	return (int64_t)ts.tv_sec * 1000000000 + (int64_t)ts.tv_nsec;
}

static inline int x_y_to_flat(
	int x,
	int y,
	int width,
	int height,
	int row_major
) {
	return row_major ? (y * width + x) : (x * height + y);
}

// called whenever we disconnect from the server
static void _disconnect_cb(__attribute__((unused)) int ch, __attribute__((unused)) void* context)
{
	printf("Server Disconnected\n");
	return;
}

static void _connect_cb(__attribute__((unused)) int ch, __attribute__((unused)) void* context)
{
	printf("connected\n");
	return;
}


void do_preprocessing(const cv::Mat& img)
{

	// smooth out input image
	GaussianBlur(img, img, cv::Size(5, 5), 0);

	// Resize the image
	int scale = 32;
	cv::Mat resizedImg;
	cv::resize(img, resizedImg, cv::Size(img.cols/scale, img.rows/scale));

	// blur the downsized image
	cv::Mat blurredImg;
	cv::GaussianBlur(resizedImg, blurredImg, cv::Size(9, 9), 0);

	// Upscale the blurred image back to the original size
	cv::Mat upscaledBlurredImg;
	cv::resize(blurredImg, upscaledBlurredImg, img.size());

	// Convert upscaled blurred image to 16-bit
	cv::Mat upscaledBlurred16bit;
	upscaledBlurredImg.convertTo(upscaledBlurred16bit, CV_16U, 2);  // Convert blurred image to 16-bit

	// Convert the original image to 16-bit
	cv::Mat img16bit;
	img.convertTo(img16bit, CV_16U, 256);  // Convert original image to 16-bit

	// Perform element-wise division (note: OpenCV does not support direct division of different types)
	cv::Mat dividedImg;
	cv::divide(img16bit, upscaledBlurred16bit, dividedImg, 1.0, CV_16U);  // Using CV_16U to keep results in 16-bit space

	// Normalize to 8-bit range
	dividedImg.convertTo(img, CV_8U, 1.0);

	return;
}


void print_struct_hex(const void *ptr, size_t size) {
    const unsigned char *byte_ptr = (const unsigned char *)ptr;

    for (size_t i = 0; i < size; i++) {
        printf("%02X ", byte_ptr[i]);  // Print each byte in hex, zero-padded
    }
    printf("\n");
}


// camera helper callback whenever a frame arrives
static void _helper_cb(__attribute__((unused)) int ch, camera_image_metadata_t meta, char* frame, __attribute__((unused)) void* context)
{
	// flush out the channel if it's backed up, usually right after connecting
	if(pipe_client_bytes_in_pipe(ch)>meta.size_bytes*3){
		printf("flushing backed up pipe ch %d\n", ch);
		pipe_client_flush(ch);
		return;
	}

	if(meta.format != IMAGE_FORMAT_RAW8){
		fprintf(stderr, "this test code only works witth 8-bit images\n");
		return;
	}


	static int has_init = 0;
	if(!has_init && _init(meta.width, meta.height)) return;
	has_init = 1;


	// convert to opencv mat for convenience
	// this does not memcopy the image, only wrap it with an opencv header
	cv::Mat img(meta.height, meta.width, CV_8UC1, frame);

	if(en_timing) printf("\n");

	// do the pre-processing step, only needed on image stream that are not normalized already
	if(en_preprocessing){
		int64_t start = _apps_time_monotonic_ns();
		do_preprocessing(img);
		//cv::equalizeHist(img, img);
		int64_t end = _apps_time_monotonic_ns();
		if(en_timing) printf("preprocess:   %6.2fms\n", (double)(end - start) / 1000000);
	}


	// run the pyramid downscaler process
	int64_t start = _apps_time_monotonic_ns();
	mcv_cvp_pyr_process(pyr_handle, (uint8_t*)frame, &pyr_output);
	int64_t end = _apps_time_monotonic_ns();
	if(en_timing) printf("pyr:          %6.2fms\n", (double)(end - start) / 1000000);


	start = _apps_time_monotonic_ns();
	static int img_init = 0;

	// run the optic flow process with prev img in memory
	if (img_init) {
		mcv_cvp_opt_flow_process_from_mem(
			opt_flow_handle,
			pyr_output.image_pyramids[optic_flow_octave],
			&opt_flow_output);
	} else {
		mcv_cvp_opt_flow_process_from_img(
			opt_flow_handle,
			pyr_output.image_pyramids[optic_flow_octave],
			pyr_output.image_pyramids[optic_flow_octave],
			&opt_flow_output);
		img_init = 1;
	}

	end = _apps_time_monotonic_ns();
	//if(en_timing) printf("opt flow:     %6.2fms\n", (double)(end - start) / 1000000);
	printf("opt flow:     %6.2fms\n", (double)(end - start) / 1000000);


	// no need to draw the overlay if there are no clients
	if(pipe_server_get_num_clients(0)<1) return;


	// draw the overlay
	start = _apps_time_monotonic_ns();
	cv::Mat overlay(meta.height, meta.width,
					CV_8UC1, pyr_output.image_pyramids[0]);
	cv::cvtColor(overlay, overlay, cv::COLOR_GRAY2RGB);

	int n_8x8_rows = (overlay.rows >> optic_flow_octave) / 8;
	int n_8x8_cols = (overlay.cols >> optic_flow_octave) / 8; // 8x8
	int scale_factor = std::pow(2, optic_flow_octave);

	int max = 0;
	int min = 99999;


	for (int row_i = 0; row_i < n_8x8_rows; row_i++) {
		for (int col_i = 0; col_i < n_8x8_cols; col_i++) {

			int arrow_start_x = (col_i * 8 + 4);
			int arrow_start_y = (row_i * 8 + 4);
			int coord         = x_y_to_flat(col_i, row_i, n_8x8_cols, n_8x8_rows, 1);

			int arrow_end_x   = (arrow_start_x - opt_flow_output.mvs[coord].mv_x);
			int arrow_end_y   = (arrow_start_y - opt_flow_output.mvs[coord].mv_y);

			// // conf in the mvs struct is useless, use the patch variance instead
			// // to get a sense of if there was texture to track
			// int conf = opt_flow_output.stats[coord].variance;
			// if(conf>255) conf = 255;
			// cv::Scalar mv_color(255-conf, conf, 0);

			// color based on confidence
			int conf = opt_flow_output.mvs[coord].conf;
			if(conf<min) min = conf;
			if(conf>max) max = conf;
			int red = 255;
			int green = 0;
			int blue = 0;

			if(conf>=0){
				red = 127 - conf*8;
				green = 127 + conf*8;
			}
			cv::Scalar mv_color(red, green, blue);


			cv::line(
				overlay,
				cv::Point2f(arrow_start_x * scale_factor, arrow_start_y * scale_factor),
				cv::Point2f(arrow_end_x * scale_factor, arrow_end_y * scale_factor),
				mv_color, 3, 1);
		}
	}
	end = _apps_time_monotonic_ns();
	if(en_timing) printf("draw_overlay: %6.2fms\n", (double)(end - start) / 1000000);


	printf("min %6d max %6d\n", min, max);

	camera_image_metadata_t color_meta = meta;
	color_meta.format = IMAGE_FORMAT_RGB;
	color_meta.size_bytes = color_meta.size_bytes * 3;
	pipe_server_write_camera_frame(0, color_meta, overlay.data);

	return;
}




int main(int argc, char* argv[])
{
	// check for options
	if(_parse_opts(argc, argv)) return -1;

	// set some basic signal handling for safe shutdown.
	// quitting without cleanup up the pipe can result in the pipe staying
	// open and overflowing, so always cleanup properly!!!
	enable_signal_handler();
	set_cpu_affinity(cpu_set_big_cores());
	print_cpu_affinity();
	main_running = 1;


	// set up all our MPA callbacks
	pipe_client_set_camera_helper_cb(0, _helper_cb, NULL);
	pipe_client_set_disconnect_cb(0, _disconnect_cb, NULL);
	pipe_client_set_connect_cb(0, _connect_cb, NULL);
	printf("waiting for server at %s\n", pipe_path);
	pipe_client_open(0, pipe_path, PROCESS_NAME, EN_PIPE_CLIENT_CAMERA_HELPER, 0);


	// keep going until the  signal handler sets the running flag to 0
	while(main_running) usleep(500000);

	// all done, signal pipe read threads to stop
	pipe_client_close_all();
	pipe_server_close_all();
	printf("\nclosing and exiting\n");

	return 0;
}
