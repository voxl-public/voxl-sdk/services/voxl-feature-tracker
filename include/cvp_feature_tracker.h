/*******************************************************************************
 * Copyright 2024 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

/**
 * @headerfile  cvp_feature_tracker.h 
 * @brief       Defines functionality to invoke the CVP Feature Tracker as 
 *              defined in libmodal-cv.
 * 
 * @author      Thomas Patton (thomas.patton@modalai.com)
 */

#ifndef VOXL_CVP_FEATURE_TRACKER_H
#define VOXL_CVP_FEATURE_TRACKER_H

#include <modalcv.h>
#include "vft_interface.h"
#include "vft_common.h"

class CVPFeatureTracker {
    public:
        CVPFeatureTracker();
        ~CVPFeatureTracker();

        /**
         * @brief Initializes a CVPFeatureTracker object with params
         * 
         * If is_stereo is set to true, the CVPFeatureTracker will initialize
         * two mcv_cvp_feature_tracker_handle objects, so that both images can 
         * be processed in parallel
         * 
         * @param w Width of the input image
         * @param h Height of the input image
         * image
         */
        void init(int id, int w, int h);
        
        /**
         * @brief Core tracking function
         * 
         * Acts as a wrapper for the functionality defined to track features in 
         * libmodal-cv. This wrapper uses CVP Functionality to perform feature
         * point extraction, image pyramiding, and semi-dense optic flow plus 
         * some core logic in order to track features.
         * 
         * @param packet A packet of image data
         * @param tracked_feats Output location for tracked features
         * @param num_features_to_track Number of features to (attempt to) track
         * @return 0 if successful, -1 otherwise
         */
        int track(
            const image_data &packet,
            std::vector<vft_feature> &tracked_feats,
            int num_features_to_track
        );

        /**
         * @brief Draws an overlay of the tracked features onto a cv::Mat
         * 
         * @param current_data Current image data packet
         * @param feats_out Vector of extracted features
         * @param overlay The cv::Mat to draw on
         * @return 0 if successful, -1 otherwise
         */
        int draw_overlay( 
            image_data current_data,
            std::vector<vft_feature> feats_out,
            cv::Mat &overlay
        );

    protected:

        /**
         * @brief Handle used for invoking the feature tracker on main cb
         */
        std::vector<mcv_cvp_feature_tracker_handle> feature_tracker_handles;
        std::map<int, std::pair<int, int>> prev_selected_ids;
        std::vector<std::vector<int>> feature_distributions;

        // assoc camera/stream/dataset ID to this tracker -- introspection
        int id;
};

extern double pixel_filter_size;
extern double pixel_search_radius;
extern double octave_search_radius;
extern bool en_id_sort;


#endif // VOXL_CVP_FEATURE_TRACKER_H
