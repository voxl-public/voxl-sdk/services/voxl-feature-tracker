/*******************************************************************************
 * Copyright 2022 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

/**
 * @file        cvp_feature_tracker.cpp 
 * @brief       Defines functionality to invoke the CVP Feature Tracker as 
 *              defined in libmodal-cv.
 * 
 * @author      Thomas Patton (thomas.patton@modalai.com)
 */
#include <modalcv/opt_flow.h>
#include <thread>

#include <modalcv.h>
#include <unordered_set>

#include "cvp_feature_tracker.h"
#include "config_file.h"

CVPFeatureTracker::CVPFeatureTracker(){}

void CVPFeatureTracker::init(int tid, int w, int h) {

    id = tid;

    // create and populate a configuration object for our feature tracker
    mcv_cvp_feature_tracker_config_t feature_tracker_config;

    // which octave of the pyramid to perform optic flow on
    feature_tracker_config.opt_flow_octave = 1;

    // by default use entire image space (provide a mask of 1's or NULL)
    feature_tracker_config.feature_mask = NULL;
    feature_tracker_config.pyr_fpx_config = mcv_cvp_pyr_fpx_default_config();
    feature_tracker_config.two_img_input = false;

    // set up opt flow
    // w/h here are dictated by the octave we want to do flow on as defined
    // above in the feature_tracker_config
    feature_tracker_config.opt_flow_config.width =
        w / pow(2, feature_tracker_config.opt_flow_octave);
    feature_tracker_config.opt_flow_config.height =
        h / pow(2, feature_tracker_config.opt_flow_octave);
    feature_tracker_config.opt_flow_config.en_stats = 0;
    feature_tracker_config.opt_flow_config.input_ubwc = 0;  // pyrfpx output ubwc

    // get a handle for the feature tracker
    mcv_cvp_feature_tracker_handle handle;
    handle = mcv_cvp_feature_tracker_init(feature_tracker_config);
    if (NULL == handle) {
        fprintf(stderr, "ERROR getting cvp feature tracker handle\n");
        return;
    }
    this->feature_tracker_handles.push_back(handle);
    
    return;
}

CVPFeatureTracker::~CVPFeatureTracker(
) {
    // tear down the CVP Feature Tracker
    for(int i = 0; i < (int)this->feature_tracker_handles.size(); i++) {
        mcv_cvp_feature_tracker_deinit(this->feature_tracker_handles[i]);
    }
    return;
}

auto findElementById(const std::vector<vft_feature>& data, const vft_feature& target) {
  // Use std::find_if to find the element with matching id
  auto it = std::find_if(data.begin(), data.end(),
                         [target](const vft_feature& element) { return element.id == target.id; });

  // Check if element was found
  return it;
}

#ifdef UNDER_WORK
static std::vector<mcv_cvp_feature_tracker_output_t> last_features;
#endif


int CVPFeatureTracker::track(
    const image_data &packet,
    std::vector<vft_feature> &tracked_feats,
    int num_features_to_track
) {
    size_t img_count = packet.images.size();

    // create output structures with size according to number of images
    int16_t num_outputs[img_count];
    mcv_cvp_feature_tracker_output_t output[img_count][MAX_N_FEATURES];

// non parallelized
//    for (int i = 0; i < (int)img_count; i++){
//    	        mcv_cvp_feature_tracker_process(id, this->feature_tracker_handles[i],
//    	            packet.images[i].data, &num_outputs[i], output[i]);
//    }


    // invoke CVP feature tracker with threads if needed
    if (img_count == 1) {
        mcv_cvp_feature_tracker_process(id, this->feature_tracker_handles[0],
            packet.images[0].data, nullptr, 
            num_features_to_track,  (void*)&this->prev_selected_ids, (void*)&this->feature_distributions, &num_outputs[0], output[0]);

    } else {
        std::vector<std::thread> threads_vec;
        for (int i = 0; i < (int)img_count; i++) {
            threads_vec.push_back(std::thread(mcv_cvp_feature_tracker_process, id,
                this->feature_tracker_handles[i], packet.images[i].data, nullptr, 
                num_features_to_track, (void*)&this->prev_selected_ids, (void*)&this->feature_distributions, &num_outputs[i],
                output[i]));
        }

        // rejoin threads
        for (int i = 0; i < (int)img_count; i++){
            threads_vec[i].join();
        }
    }

    // iterate through the output features, adding them to tracked_feats
    std::vector<vft_feature> tmp_tracked_feats;

    for(int img_idx = 0; img_idx < (int)img_count; img_idx++) {
        int N = MIN(num_features_to_track, num_outputs[img_idx]); // bound number of outputs

        mcv_cvp_feature_tracker_output_t* vft_publish_output = output[img_idx];

        for(int output_i = 0; output_i < N; output_i++) {
			vft_feature feature_data;
            feature_data.id           = vft_publish_output[output_i].id;
            feature_data.x            = vft_publish_output[output_i].x;
            feature_data.y            = vft_publish_output[output_i].y;
            feature_data.x_prev       = vft_publish_output[output_i].x_prev;
            feature_data.y_prev       = vft_publish_output[output_i].y_prev;
            feature_data.age          = vft_publish_output[output_i].age;
            feature_data.pyr_lvl_mask = vft_publish_output[output_i].pyr_lvl_mask;
            feature_data.score        = vft_publish_output[output_i].score;
			feature_data.cam_id       = packet.tracker_ids[img_idx];
			tmp_tracked_feats.push_back(feature_data);
		}
	}


    tracked_feats.clear();
    // // replace
    std::swap(tracked_feats, tmp_tracked_feats);

    return 0;
}



int CVPFeatureTracker::draw_overlay( 
    image_data current_data,
    std::vector<vft_feature> feats_out,
    cv::Mat &overlay
) {

    int n_images = (int)current_data.images.size();
    if(n_images == 1) {
        cv::Mat current_frame = current_data.images[0];
        cv::cvtColor(current_frame, overlay, cv::COLOR_GRAY2RGB);
        for(int i = 0; i < (int)feats_out.size(); i++) {
            int age = feats_out[i].age;
            int radius, thickness;

            if(age > 60) {
                radius = 4;
                thickness = 3;
            } else {
                radius = 2;
                thickness = 2;
            }
            //cv::Scalar color(std::max(0, 255-5*age), 255, 0);
            int blue = 0;
            int red = 255-(age*3);
            if(red<0) red = 0;
            int green = std::min(255, age);
            cv::Scalar color(red, green, blue);
            cv::Rect rect(feats_out[i].x-radius, feats_out[i].y-radius, radius*2, radius*2);
            cv::rectangle(overlay, rect, color, thickness, 1, 0);

            // if (active_cam >= 0  && i < 20)
            {
                int baseline = 0;
                float font_size = 0.5;
				char str[32];
				sprintf(str, "%d", (int)feats_out[i].id);
				cv::Size text_size = cv::getTextSize(str, cv::FONT_HERSHEY_COMPLEX,
						font_size, font_size, &baseline);

				cv::putText(
						overlay, //target image
						str, //text
						cv::Point((feats_out[i].x - radius - text_size.width),
								feats_out[i].y - radius-text_size.height),
						cv::FONT_HERSHEY_COMPLEX, font_size,
						cv::Scalar(255, 255, 255), //font color
						1.0, cv::LINE_AA);
            }

        }
    } else { // 2 images (or more?)
        std::vector<cv::Mat> inputs;
        int offset = std::min(current_data.tracker_ids[0], current_data.tracker_ids[1]);
        for(int img_i = 0; img_i < n_images; img_i++) {
            cv::Mat current_frame = current_data.images[img_i];
            cv::cvtColor(current_frame, current_frame, cv::COLOR_GRAY2RGB);

            for(int i = 0; i < (int)feats_out.size(); i++) {
                if(feats_out[i].cam_id - offset != img_i) continue;    // pt not for this camera frame
                int age = feats_out[i].age;
                int radius, thickness;
                if(age > 15) {
                    radius = 4;
                    thickness = 3;
                } else {
                    radius = 1;
                    thickness = 1;
                }
                cv::Scalar color(std::max(0, 255-5*age), 255, 0);
                cv::Rect rect(feats_out[i].x-radius, feats_out[i].y-radius, radius*2, radius*2);
                cv::rectangle(current_frame, rect, color, thickness, 1, 0);

                // if (active_cam >= 0  && i < 20)
                // {
                // 	char str[32];
				// 	int baseline = 0;
				// 	float font_size = 1.25;
				// 	sprintf(str, "%d(%d)", (int)feats_out[i].id, (int)feats_out[i].age);
				// 	cv::Size text_size = cv::getTextSize(str, cv::FONT_HERSHEY_COMPLEX,
				// 			font_size, font_size, &baseline);

				// 	cv::putText(
				// 			overlay, //target image
				// 			str, //text
				// 			cv::Point((feats_out[i].x - radius - text_size.width),
				// 					feats_out[i].y - radius-text_size.height),
				// 			cv::FONT_HERSHEY_COMPLEX, font_size,
				// 			cv::Scalar(255, 255, 255), //font color
				// 			2.0, cv::LINE_AA);
                // }

            }
            inputs.push_back(current_frame);
        }
        cv::vconcat(inputs, overlay);
    }
    return 0;
}
