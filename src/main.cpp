/*******************************************************************************
 * Copyright 2024 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

/*
 * @file        main.cpp
 *
 * @brief       Driver code for running voxl-feature-tracker
 *
 *              There are two primary modes for running this feature tracker.
 *              The first is the OpenCV implementation which makes use of the
 *              aforementioned library to recognize feature points and track
 *              them. The second mode is the CVP implementation which uses
 *              logic defined in /core-libs/libmodal-cv to do the same task.
 *              This latter implementation offloads core logic to onboard
 *              discrete signal processors and is thus generally more
 *              computationally and thermally efficient. However the performance
 *              is marginally worse.
 *
 *              Configuration for this module can be found in
 *              /etc/modalai/voxl-feature-tracker.conf . In addition to choosing
 *              a feature tracker mode for each camera you can also choose a
 *              number of features to (ideally) track.
 *
 * @author      Thomas Patton (thomas.patton@modalai.com)
 * @date        2024
 */

#include <getopt.h>
#include <modal_journal.h>
#include <modal_pipe.h>
#include <modal_pipe_interfaces.h>
#include <modal_pipe_server.h>
#include <modalcv/common.h>
#include <voxl_common_config.h>
#include <opencv2/core/hal/interface.h>
#include <unistd.h>

#include <atomic>
#include <chrono>
#include <condition_variable>
#include <functional>
#include <mutex>
#include <opencv2/features2d.hpp>
#include <thread>
#include <unordered_map>
#include <tuple>

#include "command_interface.h"
#include "config_file.h"
#include "cvp_feature_tracker.h"
#include "vft_common.h"


#define PROCESS_NAME "voxl-feature-tracker"

// server pipes we publish
#define FEATURE_PIPE_CH	0
#define FEATURE_LOCATION MODAL_PIPE_DEFAULT_BASE_DIR VFT_PIPE_NAME "/"

#define OVERLAY_PIPE_CH	1
#define OVERLAY_NAME	"feature_tracker_overlay"
#define OVERLAY_LOCATION MODAL_PIPE_DEFAULT_BASE_DIR OVERLAY_NAME "/"

#define OUTPUT_PIPE_SIZE (16*1024*1024)

/**
 * Variables to describe the state of the module
 */
std::mutex state_mutex;
std::atomic<vft_state> overall_state;

/**
 * Configuration variables for specialized run modes
 */
static int en_debug;
static int en_debug_pub;
static bool en_timing;
static bool en_config_only;
static int active_cam = -1;

// vio cam config from common vio_cams.conf
static int n_cams;
static vio_cam_t vio_cams[VFT_MAX_GROUP_CAMS];

/**
 * This vector holds the most recent callback times for the tracker. Logic
 * inside _camera_helper_cb insures this vector doesn't grow above 500 in
 * length. This vector is then used to compute the mean callback time for
 * logging purposes
 */
static std::vector<int64_t> callback_times;
static int64_t frame_counter[VFT_MAX_GROUP_CAMS];
static std::vector<int> dropped_frames[VFT_MAX_GROUP_CAMS];

/**
 * These vectors hold the trackers with one tracker for each distinct camera
 * channel. So if a camera is publishing on channel i and is specified to be a
 * CVP tracker, then cvp_tracker_vec will have an initialized tracker at index
 * (i). The only exception to this is when "both" is
 * specified as a tracking option, in which case both vectors will have a
 * tracker initialized at that index
 */
static std::vector<CVPFeatureTracker> cvp_tracker_vec(VFT_MAX_GROUP_CAMS);

/**
 * @brief Defines a publisher for a given camera group
 */
struct camera_group_publisher_t {

	int initialized;

	// mtx and cond for r/w on this camera group
	std::mutex pub_mtx;
	std::condition_variable pub_cond;

	// feature packet / features for this group
	vft_feature_packet feature_packet;
	std::vector<std::vector<vft_feature>> feature_data;

	// overlay stuff
	std::mutex overlay_mtx;
	std::vector<cv::Mat> overlays;

	// timestamps for this group
	std::vector<int64_t> group_timestamps;

	// if this group is ready to publish
	bool group_should_publish;

	// index of active cameras expected to publish data
	std::vector<int> active_cams;

	// the last frame_id published for each frame (to avoid duplicate publishes)
	std::vector<int> has_new_data;
};


/**
 * Structs for each camera group publisher
 */
static std::thread publisher_thread;
static camera_group_publisher_t publisher;


/**
 * @brief Print usage for voxl-feature-tracker
 */
static void _print_usage(void) {
	printf(
		"\n\
This is meant to run in the background as a systemd service, but can be\n\
run manually with the following debug options\n\
\n\
-c, --config                only parse the config file and exit, don't run\n\
-d, --debug                 enable debug prints in libmodal_cv\n\
-e, --debug_publish         debug prints specific to publishing\n\
-t, --timing                enable timing stats dump to console if debug is enabled\n\
-h, --help                  print this help message\n\
\n");
	return;
}

/**
 * @brief Parse user provided configuration options
 */
static bool _parse_opts(int argc, char *argv[]) {
	static struct option long_options[] = {
										   {"config", no_argument, 0, 'c'},
										   {"debug", no_argument, 0, 'd'},
										   {"debug_publish", no_argument, 0, 'e'},
										   {"timing", no_argument, 0, 't'},
										   {"db", required_argument, 0, 'v'},
										   {"help", no_argument, 0, 'h'},
										   {"sw_sync", no_argument, 0, 's'},
										   {0, 0, 0, 0}
											};

	while (1) {
		int option_index = 0;
		int c = getopt_long(argc, argv, "cdehtv:s", long_options, &option_index);

		// Detect the end of the options.
		if (c == -1) {
			break;
		}

		switch (c) {
			case 0:
				// for long args without short equivalent that just set a flag
				// nothing left to do so just break.
				if (long_options[option_index].flag != 0) break;
				break;

			case 'c':
				en_config_only = 1;
				break;

			case 'd':
				printf("Enabling debug mode\n");
				en_debug = 1;
				mcv_en_debug_prints(1);
				break;

			case 'e':
				en_debug_pub = 1;
				break;

			case 't':
				en_timing = 1;
				mcv_en_timing_prints(1);
				break;

			case 'h':
				_print_usage();
				return true;

			case 's':
				en_stereo_sync_ts = true;
				break;

			case 'v':
				active_cam = static_cast<int8_t>(std::atoi(optarg));
				break;

			default:
				_print_usage();
				return true;
		}
	}
	return false;
}


/**
 * Determines if this camera group is ready to publish based on camera
 * timestamps
 */
static void should_group_publish(int64_t this_timestamp, int camera_i)
{
	vft_feature_packet feature_packet =
		publisher.feature_packet;
	int n_group_cams = feature_packet.n_cams;

	// reset cams that need to be published
	publisher.feature_packet.cams_with_data = 0;

	// validates if all cameras actually have new data
	for (int i = 0; i < n_group_cams; i++) {

		// get camera index
		int cam_i = publisher.active_cams[i];

		// check if the camera has new data
		bool new_data = publisher.has_new_data[cam_i];

		// camera missing data entirely, not ready
		if(new_data == false){
			publisher.group_should_publish = false;
			if(en_debug_pub){
				printf("checking pub ready for cam %d, other cams missing data\n", camera_i);
			}
			return;
		}

		// other cam has data but is too old, need to wait
		// TODO catch case where a camera crashes
		int64_t diff = abs(this_timestamp - publisher.group_timestamps[cam_i]);
		if(diff > 15000000){
			publisher.group_should_publish = false;
			if(en_debug_pub){
				printf("checking pub ready for cam %d, other cam has old data\n", camera_i);
			}
			return;
		}

		// if there's new data, set the camera bit in the feature_packet struct
		if (new_data) {
		publisher.feature_packet.cams_with_data
			|= (1U << cam_i);
		}
	}

	if(en_debug_pub){
		printf("checking pub ready for cam %d, ready to publish!!!\n", camera_i);
	}
	publisher.group_should_publish = true;
}


static void publisher_thread_fn(void)
{
	printf("starting publisher thread\n");

	std::unique_lock<std::mutex> lock(publisher.pub_mtx);
	while (overall_state == RUNNING) {
		publisher.pub_cond.wait(lock, [&] {
			return publisher.group_should_publish ||
				   overall_state == STOPPING;  // need to cover stopping case
		});

		// on shutdown, close out the thread gracefully
		if (overall_state == STOPPING) return;


		if (en_stereo_sync_ts)
		{
			// TBD
			publisher.feature_packet.timestamp_ns[0] =
				publisher.group_timestamps[0];
		}
		else {
			for (int j=0; j<publisher.feature_packet.n_cams; j++) {
				int cam_i = publisher.active_cams[j];

				// set timestamp of publish, rely on 0 camera for now
				publisher.feature_packet.timestamp_ns[cam_i] =
					publisher.group_timestamps[cam_i];
			}
		}

		// loop over the active cams and check if they have data to publish
		std::vector<int> cams_w_data;
		for (int i = 0; i < (int)publisher.active_cams.size(); i++) {
			int cam_i = publisher.active_cams[i];
			if (publisher.has_new_data[cam_i] &&
				publisher.feature_packet.num_feats[cam_i] > 0) {
				cams_w_data.push_back(cam_i);
			}
		}

		// number of buffers is made up of one for the feature_packet struct and
		// then one additional buf for each camera that has features to publish
		int n_buffers = 1 + cams_w_data.size();
		if(en_debug_pub) printf("filling %d buffers\n", n_buffers);

		// buffers and lengths which are populated and then written to the pipe
		const void *buffers[n_buffers];
		size_t lengths[n_buffers];

		// set buffer pointer and byte size of the feature packet
		buffers[0] = &publisher.feature_packet;
		lengths[0] = sizeof(vft_feature_packet);

		// loop over each buffer and set the appropriate buf pointer and byte
		// length for each active camera with features to publish
		for (int i = 1; i < n_buffers; i++) {
			int cam_i = cams_w_data[i - 1];
			buffers[i] = publisher.feature_data[cam_i].data();
			if(buffers[i]==NULL){
				fprintf(stderr, "ERROR feature data has null data pointer cam #%d\n", i);
			}
			lengths[i] = sizeof(vft_feature) *
						 publisher.feature_packet.num_feats[cam_i];
		}

		if(en_debug_pub){
			fprintf(stderr,"publishing features for %d cams to pipe\n", (int)publisher.active_cams.size());
		}

		// try to write feature_packet and features data to pipe
		if (n_buffers > 1) {
			pipe_server_write_list(FEATURE_PIPE_CH, n_buffers, buffers, lengths);
		}

		// turn off new data bit for cams
		for (int i = 0; i < (int)publisher.active_cams.size(); i++) {
			int cam_i = publisher.active_cams[i];
			publisher.has_new_data[cam_i] = 0;
		}
		publisher.group_should_publish = false;


		// overlay publishing after feature point publishing
		// this is completely independent and turns on and off based on if there
		// is a subscriber or not, just for debugging.
		std::unique_lock<std::mutex> lock2(publisher.overlay_mtx);
		if(!publisher.overlays.empty()){
			cv::Mat result;

			// not all cameras may be present, so clean out empty slots in the vector
			// so opencv doesn't complain of empty images in vconcat()
			publisher.overlays.erase(std::remove_if(publisher.overlays.begin(), publisher.overlays.end(),
				[](cv::Mat x) { return x.cols  == 0; }), publisher.overlays.end());
			publisher.overlays.shrink_to_fit();

			// concat images together
			cv::vconcat(publisher.overlays, result);
			if(en_debug_pub){
				printf("concatenating %d overlays\n", (int)publisher.overlays.size());
			}

			// if successful, publish
			if (!result.empty() ) {
				camera_image_metadata_t draw_meta;
				draw_meta.magic_number = CAMERA_MAGIC_NUMBER;
				draw_meta.format = IMAGE_FORMAT_RGB;
				draw_meta.width = result.cols;
				draw_meta.height = result.rows;
				draw_meta.size_bytes = draw_meta.width * draw_meta.height * 3;
				draw_meta.timestamp_ns = publisher.feature_packet.timestamp_ns[0];
				draw_meta.frame_id = 0;
				if (sizeof(result.data) > 0){
					pipe_server_write_camera_frame(OVERLAY_PIPE_CH, draw_meta, (char *)result.data);
				}
			}
			// clear the vector so we know we are starting fresh, also makes
			// sure we don't get a lingering image if a camera feed drops
			publisher.overlays.clear();
			publisher.overlays.shrink_to_fit();
		}
		lock2.unlock();



	} // end of big while loop
	return;
}


/**
 * @brief Exits the program cleanly
 */
static void _quit(int ret)
{
	if (en_debug) printf("Attempting to quit...\n");
	overall_state = STOPPING;

	// Close all the open pipe connections
	pipe_server_close_all();
	pipe_client_close_all();

	if (en_debug) printf("Closed server and client\n");

	// need to signal waiting cond vars so they can return
	publisher.pub_cond.notify_one();
	publisher_thread.join();
	if (en_debug) printf("Closed threads\n");

	// remove this process ID file
	remove_pid_file(PROCESS_NAME);
	if (en_debug) printf("removed pid file\n");

	if (ret == 0)
		printf("Exiting Cleanly\n");
	else
		printf("error code %d\n", ret);
	exit(ret);
	return;
}


static void _camera_disconnect_cb(__attribute__((unused)) int ch,
								  __attribute__((unused)) void *context) {
	// TODO remove from active cams list so we can keep publishing is one cam crashes
	fprintf(stderr, "Disconnected from camera pipe\n");
}


static void _feature_connect_cb(__attribute__((unused)) int ch,
								__attribute__((unused)) int client_id,
								__attribute__((unused)) char *name,
								__attribute__((unused)) void *context) {
	printf("Client connected to feature tracker\n");
}

/**
 * @brief Primary callback for ingestion of camera frames
 *
 * @param ch The pipe channel the camera frame was ingested on
 * @param meta Metadata for the camera
 */
static void _camera_helper_cb(int ch, camera_image_metadata_t meta, char *frame,
							  void *context) {

	auto start = std::chrono::high_resolution_clock::now();

	// flush out the channel if it's backed up, usually right after connecting
	if(pipe_client_bytes_in_pipe(ch)>0){
		printf("flushing backed up pipe ch %d\n", ch);
		pipe_client_flush(ch);
		return;
	}

	if(en_debug_pub){
		fprintf(stderr, "received cam ch %d delay: %0.1fms\n", ch, (_apps_time_monotonic_ns()-meta.timestamp_ns)/1000000.0);
	}

	state_mutex.lock();
	if(overall_state != RUNNING) {
		state_mutex.unlock();
		if (en_debug) printf("not running yet, exiting cam helper cb\n");
		return;
	}
	state_mutex.unlock();

	image_data curr_packet;
	curr_packet.timestamp_ns = meta.timestamp_ns;
	curr_packet.tracker_ids.push_back(ch);

	if( meta.format != IMAGE_FORMAT_RAW8		&&
		meta.format != IMAGE_FORMAT_NV12		&&
		meta.format != IMAGE_FORMAT_NV21		&&
		meta.format != IMAGE_FORMAT_STEREO_RAW8	&&
		meta.format != IMAGE_FORMAT_STEREO_NV12	&&
		meta.format != IMAGE_FORMAT_STEREO_NV21)
	{
		M_ERROR("ERROR received unsupported image format %d\n", meta.format);
		_quit(-1);
	}

	// Unpack the data into an opencv image Mat
	cv::Mat img(meta.height, meta.width, CV_8UC1, frame);

	// do any prepossessing needed
	if(en_blur) cv::GaussianBlur(img, img, cv::Size(3, 3), 0);
	if(en_normalization) cv::equalizeHist(img, img);

	// TODO figure out image masking
	// static cv::Mat mask(meta.height, meta.width, CV_8UC1, cv::Scalar(0));

	curr_packet.images.push_back(img);

	// run the tracker!!
	cvp_tracker_vec[ch].track(
			curr_packet,
			publisher.feature_data[ch],
			num_features);

	if(en_debug_pub) fprintf(stderr, "done tracking cam %d\n", ch);

	// if overlay is needed, draw it
	if(pipe_server_get_num_clients(OVERLAY_PIPE_CH)>0){

		std::unique_lock<std::mutex> lock2(publisher.overlay_mtx);

		cv::Mat overlay;
		cvp_tracker_vec[ch].draw_overlay(curr_packet,publisher.feature_data[ch], overlay);

		// push our new overlay to the vector
		if(publisher.overlays.size()<VFT_MAX_GROUP_CAMS){
			publisher.overlays.resize(VFT_MAX_GROUP_CAMS);
		}

		publisher.overlays.at(ch) = overlay;
		if(en_debug_pub){
			printf("pushed overlay for cam index %d\n", ch);
		}
	}


	// lock current r/w for camera group then write data
	{
		std::unique_lock<std::mutex> lock(publisher.pub_mtx);

		// pass along number of features to the packet + magic num
		publisher.feature_packet.num_feats[ch] = (int)publisher.feature_data[ch].size();
		publisher.feature_packet.magic_number = VOXL_FT_MAGIC_NUMBER;
		publisher.feature_packet.frame_ids[ch] = frame_counter[ch];
		publisher.has_new_data[ch] = 1;

		// add current timestamp, then check if we should publish
		// per james, should be timestamp + 1/2 exposure
		int64_t offset_time = meta.timestamp_ns + meta.exposure_ns / 2;
		publisher.group_timestamps[ch] = offset_time;
		should_group_publish(offset_time, ch);

		if(frame_counter[ch] != 0){
			publisher.pub_cond.notify_one();
		}
	}

	// incr the frame counter for this camera
	frame_counter[ch] += 1;

	if (en_timing) {
		// timing stuff
		auto end = std::chrono::high_resolution_clock::now();
		auto duration =
			std::chrono::duration_cast<std::chrono::microseconds>(end - start)
				.count();
		if (callback_times.size() > 500) {
			callback_times.erase(callback_times.begin());
		}
		callback_times.push_back(duration);

		int time_sum = 0;
		for (const auto &time : callback_times) {
			time_sum += time;
		}

		// Convert microseconds to milliseconds and calculate the running
		// average
		double mean_time_ms =
			static_cast<double>(time_sum) / callback_times.size() / 1000.0;

		// Print the mean time in milliseconds with two decimal places
		std::cout << std::fixed << std::setprecision(2)
				  << "Mean Average Callback Time :: " << mean_time_ms << " ms"
				  << std::endl;
	}
}



static void _camera_connect_cb(__attribute__((unused)) int ch,
							   __attribute__((unused)) void *context)
{
	// get the pipe info associated with this channel
	printf("Connected to cam %d %s\n", ch, vio_cams[ch].pipe_for_tracking);

	// disable connect callabck so we don't init a camera twice when camera server restarts
	pipe_client_set_connect_cb(ch, NULL, NULL);

	// grab some camera information
	int width, height;
	cJSON *info = pipe_client_get_info_json(ch);
	json_fetch_int_with_default(info, "width", &width, 1280);
	json_fetch_int_with_default(info, "height", &height, 800);

	// create the corresponding tracker
	cvp_tracker_vec[ch].init(ch, width, height);

	// lock the publisher mutex since we are about to do some config/setup of it
	std::unique_lock<std::mutex> lock(publisher.pub_mtx);

	// sort the active cams as they go in so it's easier to publish in order later
	publisher.active_cams.push_back(ch);
	std::sort(publisher.active_cams.begin(), publisher.active_cams.end());
	publisher.feature_packet.n_cams += 1;

	// only continue to set up publisher pipe stuff (and some other) in first pass
	if(publisher.initialized)	return;

	// only want to do this once per group
	for(int i=0; i<VFT_MAX_GROUP_CAMS; i++) {
		publisher.has_new_data.push_back(0);
		publisher.group_timestamps.push_back(0);
		publisher.feature_data.push_back(
			std::vector<vft_feature>());
	}
	publisher.group_should_publish = false;


	// feature output pipe
	pipe_info_t pipe_info_0 = { \
		VFT_PIPE_NAME,				// name
		FEATURE_LOCATION,			// location
		"vft_feature_packet",		// type
		PROCESS_NAME,				// server_name
		OUTPUT_PIPE_SIZE,			// size_bytes
		0							// server_pid
	};
	if(pipe_server_create(FEATURE_PIPE_CH, pipe_info_0, 0)) {
		fprintf(stderr, "ERROR creating feature pipe\n");
		_quit(-1);
	}
	pipe_server_set_connect_cb(FEATURE_PIPE_CH, _feature_connect_cb, NULL);


	// overlay output pipes
	pipe_info_t pipe_info_1 = { \
		OVERLAY_NAME,				// name
		OVERLAY_LOCATION,			// location
		"camera_image_metadata_t",	// type
		PROCESS_NAME,				// server_name
		OUTPUT_PIPE_SIZE,			// size_bytes
		0							// server_pid
	};
	if(pipe_server_create(OVERLAY_PIPE_CH, pipe_info_1, 0)) {
		fprintf(stderr, "ERROR creating overlay pipe\n");
		_quit(-1);
	}

	printf("=========================================\n");
	printf("created output pipes:\n");
	printf("Feature Pipe: %s\n", pipe_info_0.name);
	printf("Overlay Pipe: %s\n", pipe_info_1.name);
	printf("=========================================\n");

	// invoke publisher thread for this group
	publisher_thread = std::thread(publisher_thread_fn);
	publisher.initialized = 1;
	return;
}



int main(int argc, char *argv[])
{
	// Parse the command line options and terminate if the parser says we should
	// terminate
	if (_parse_opts(argc, argv)) {
		return -1;
	}

	// Load the config file
	if (config_file_read() < 0) {
		fprintf(stderr, "ERROR %d\n", config_file_read());
		_quit(-1);
	}
	if (en_config_only) return 0;


	// load only enabled vio cams from common vio-cam config file
	// no need to check for extrinsics or cam cal, we don't need those
	n_cams = vcc_read_vio_cam_conf_file(vio_cams, VFT_MAX_GROUP_CAMS, 1);
	if(n_cams<1) return -1;
	printf("vio_cam config:\n");
	vcc_print_vio_cam_conf(vio_cams, n_cams);
	printf("=================================================================\n");


	/* make sure another instance isn't running
	 * if return value is -3 then a background process is running with
	 * higher privileges and we couldn't kill it, in which case we should
	 * not continue or there may be hardware conflicts. If it returned -4
	 * then there was an invalid argument that needs to be fixed.
	 */
	if(kill_existing_process(PROCESS_NAME, 2.0) < -2) {
		fprintf(stderr, "ERROR: could not kill existing process\n");
		_quit(-1);
	}

	// start signal handler so we can exit cleanly
	if(enable_signal_handler() == -1) {
		fprintf(stderr, "ERROR: failed to start signal handler\n");
		_quit(-1);
	}

	// set up affinity for this process. right now we keep this on the small cores
	// which runs a little slower than running on the big cores by about 1.5ms
	// but lets the CPU run about 2 degrees cooler on a starling.
	// also use MEDIUM priority as this is not ultra time critical like a
	// sensor driver would be.
	set_cpu_affinity(cpu_set_small_cores());
	print_cpu_affinity();
	pipe_set_process_priority(THREAD_PRIORITY_RT_MED);

	// Set the main running flag to 1 to indicate that we are running
	main_running = 1;

	// for small images, opencv usually runs faster without parallelization
	// especially for simple things like blur. Camera streams are already
	// distributed across different threads.
	cv::setNumThreads(1);

	/* make PID file to indicate your project is running
	 * due to the check made on the call to rc_kill_existing_process() above
	 * we can be fairly confident there is no PID file already and we can
	 * make our own safely.
	 */
	make_pid_file(PROCESS_NAME);

	cvp_set_pixel_search_radius(pixel_search_radius);
	cvp_set_octave_search_radius(octave_search_radius);
	cvp_set_en_id_sort(1);

	// for each input, open the camera pipe
	for(int i=0; i<n_cams; i++) {
		pipe_client_set_disconnect_cb(i, _camera_disconnect_cb,NULL);
		pipe_client_set_connect_cb(i, _camera_connect_cb, NULL);
		pipe_client_set_camera_helper_cb(i, _camera_helper_cb, NULL);
		int flags = CLIENT_FLAG_EN_CAMERA_HELPER;
		printf("opening pipe: %s\n", vio_cams[i].pipe_for_tracking);

		if(pipe_client_open(i, vio_cams[i].pipe_for_tracking, PROCESS_NAME, flags, 0)) {
			M_ERROR("FAILED TO OPEN %s\n", vio_cams[i].pipe_for_tracking);
			return -1;
		}
	}

	// run until start/stop module catches a signal and changes main_running to 0
	printf("Setup completed! Waiting for images...\n");
	while (main_running) usleep(5000000);

	// shutdown nicely
	_quit(0);

	return 0;
}
