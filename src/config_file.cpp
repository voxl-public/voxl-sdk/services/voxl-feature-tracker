/*******************************************************************************
 * Copyright 2024 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/


#include <modal_json.h>
#include <stdio.h>
#include <voxl_common_config.h>

#include <iostream>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/core/mat.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/stereo.hpp>
#include <string>
#include <iostream>

#include "config_file.h"


#define DEFAULT_N_FEATURES 30


int num_features;
int en_blur;
int en_normalization;
int en_logging;
int en_stereo_sync_ts;
double pixel_filter_size;
double pixel_search_radius;
double octave_search_radius;
int fpx_threshold;
int fpx_robustness;
int fpx_score_shift;





int config_file_read(void)
{
	// if config file does not exist, make one and initialize it with a header
	int ret = json_make_empty_file_with_header_if_missing(CONFIG_FILE, CONFIG_FILE_HEADER);
	if (ret < 0) return -1;
	if (ret > 0) fprintf(stderr, "Creating new config file: %s\n", CONFIG_FILE);

	// get parent JSON for reading
	cJSON *parent = json_read_file(CONFIG_FILE);
	if (parent == NULL) return -1;

	json_fetch_int_with_default(	parent, "num_features",				&num_features,				DEFAULT_N_FEATURES);
	json_fetch_bool_with_default(	parent, "en_blur",					&en_blur,					0);
	json_fetch_bool_with_default(	parent, "en_normalization",			&en_normalization,			0);
	json_fetch_bool_with_default(	parent, "en_stereo_sync_ts",		&en_stereo_sync_ts,			0);
	json_fetch_double_with_default(	parent, "pixel_filter_size",		&pixel_filter_size,			2.0f);
	json_fetch_double_with_default(	parent, "pixel_search_radius",		&pixel_search_radius,		10.0f);
	json_fetch_double_with_default(	parent, "octave_search_radius",		&octave_search_radius,		3.0f);
	// json_fetch_int_with_default(	parent, "fpx_threshold",			&fpx_threshold,				20);
	// json_fetch_int_with_default(	parent, "fpx_robustness",			&fpx_robustness,			5);
	// json_fetch_int_with_default(	parent, "fpx_score_shift",			&fpx_score_shift,			8);

	// old groups field, replaced by common vio_cams.conf
	json_remove_if_present(parent, "groups");
	json_remove_if_present(parent, "version");

	// check if we got any errors in that process
	if(json_get_parse_error_flag()) {
		fprintf(stderr, "failed to parse data in %s\n", CONFIG_FILE);
		cJSON_Delete(parent);
		return -1;
	}
	if(json_get_modified_flag()) {
		printf("The JSON config file data was modified during parsing, saving the changes to disk\n");
		json_write_to_file_with_header(CONFIG_FILE, parent, CONFIG_FILE_HEADER);
	}

	cJSON_Delete(parent);

	printf("=================================================================\n");
	printf("num_features:\t\t\t%d\n",    num_features);
	printf("en_blur:\t\t\t\t%d\n",    en_blur);
	printf("en_normalization:\t\t\t%d\n",    en_normalization);
	printf("en_stereo_sync_ts:\t\t\t%d\n",    en_stereo_sync_ts);
	printf("pixel_filter_size:\t\t\t%6.3f\n", (double)pixel_filter_size);
	printf("pixel_search_radius:\t\t\t%6.3f\n", (double)pixel_search_radius);
	printf("octave_search_radius:\t\t\t%6.3f\n", (double)octave_search_radius);
	// printf("fpx_threshold: \t\t\t\t%d\n",    fpx_threshold);
	// printf("fpx_robustness: \t\t\t%d\n",    fpx_robustness);
	// printf("fpx_score_shift: \t\t\t%d\n",    fpx_score_shift);
	printf("=================================================================\n");

	return 0;

}



